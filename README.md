
[![pipeline status](https://gitlab.com/dle0/j1/badges/main/pipeline.svg)](https://gitlab.com/dle0/j1/-/commits/main)
[![Latest Release](https://gitlab.com/dle0/j1/-/badges/release.svg)](https://gitlab.com/dle0/j1/-/releases)

_j1_ converts between JSON and a linear form suited for unix tools like _grep_.

Example:

    $ echo '{"foo":[true,{"height":9.3}],"bar":null}' | j1
    foo[0]=true
    foo[1].height=9.3
    bar=null

j1 can also run in reverse. Given a series of key=value lines it
recreates the JSON value. Because later lines override earlier ones
it can be used as a crude JSON editor.

