VERSION = 0.0
CPPFLAGS += -DVERSION='"$(VERSION)"'
all: j1
check: j1 tests.sh
	./tests.sh ./j1
check-junit: tests.xml
tests.xml: j1 tests.sh
	./tests.sh -junit ./j1 >$@
clean:
	$(RM) j1 *.o
	$(RM) tests.xml

.PHONY: all clean check check-junit
