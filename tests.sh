#!/usr/bin/env bash

TMP=${TMPDIR:-/tmp}/test$$

if [ x"$1" = x"-junit" ]; then
  shift
  # A bare-bones JUnit testcase runner
  tc () {
    local desc="$1"; shift
    local caller=( $(caller) )
    echo " <testcase name='${caller[1]}:${caller[0]}: ${desc//\'/&apos;}'>"
    local exitcode
    "$@" >$TMP.out 2>&1; exitcode=$?
    if [ $exitcode -ne 0 ]; then
        echo "  <error message='exit $exitcode'/>"
	tcfailed=1
    fi
    if [ $exitcode -ne 0 -o -s "$TMP.out" ]; then
        printf '  <system-err><![CDATA['
	printf '$ %s\n' "$*"
	cat $TMP.out
	printf ']]></system-err>\n';
    fi
    rm -f $TMP.out
    echo " </testcase>"
  }
  echo "<?xml version='1.0' encoding='UTF-8'?>"
  echo "<testsuite name='${0##*/}'>"
  trap "echo '</testsuite>'; [ ! $tcfailed ] || exit 1" 0
else
  tc () {
    local desc="$1"; shift
    local caller=( $(caller) )
    if ! "$@"; then
      echo "${caller[1]}:${caller[0]}: $desc FAILED" >&2
      tcfailed=1
    fi
  }
  trap "[ ! $tcfailed ] || exit 1" 0
fi

#tc "true" true
#tc "error" false
#tc "stdout" echo something
#tc "stderr" sh -c 'echo foo >&2'

# The assertion helpers are always invoked as:
#
#    assert_[reverse_]output <JSON> [<j1_option>...] <<.
#    <LINEAR>
#    .
#

J1=${1:-./j1}
assert_output () { # input [args...]
    diff -au --label expected --label actual \
        - <(printf "%s" "$1" | $J1 "${@:2}")
}
assert_reverse_output () { # output [args...]
    diff -au --label expected --label actual \
        <(printf '%s\n' "$1") <($J1 -r "${@:2}")
}

# The actual j1 tests start here

tc "The example works" \
assert_output '{ "foo": [ true, { "height": 9.3 } ], "bar": null }' <<'.'
foo[0]=true
foo[1].height=9.3
bar=null
.

tc "Empty arrays and objects are shown" \
assert_output '[true,[false],{"q":[],"r":[{}]},5,{"x":9.7}]' <<'.'
[0]=true
[1][0]=false
[2].q=[]
[2].r[0]={}
[3]=5
[4].x=9.7
.

tc "Only keys and strings are unquoted; other literals left alone" \
assert_output '{ "a\u0009x": "b\tx", "c": 0001.2000 }' -s <<'.'
a\tx=b	x
c=0001.2000
.

tc "Unicode escapes are expanded in keys and strings" \
assert_output '{ "a\/\u0009\"\\\n": "b\/\u0009\"\\\n" }' -s <<'.'
a/\t"\\\n=b/	"\\\n
.

tc "Literals are opaque (eg 0001.2000q is invalid)" \
assert_output '{ "a\tx": "b\t\uUx", "c": 0001.2000q }' <<'.'
a\tx="b\t\uUx"
c=0001.2000q
.

tc "-t uses a tab instead of =, and emits '\t' in place of TAB" \
assert_output '{ "a	x\t": "b	x\t" }' -ts <<'.'
a\tx\t	b\tx\t
.

tc "Keys containing confusing chars are converted to \xXX form" \
assert_output '[{ "a: [0.]\t\n": 1 }]' <<'.'
[0].a:\x20\x5b0\x2e\x5d\t\n=1
.

tc "Even in -t mode, only . [ ] \t \\ \n are escaped" \
assert_output '[{ "a: [0.]\t\n": "a: [	0.]" }]' -ts <<'.'
[0].a: \x5b0\x2e\x5d\t\n	a: [\t0.]
.

tc "long unicode points and surrogate pairs are decoded to UTF-8 correctly" \
assert_output '{ "(\u28ff)": "braille", "(\uD834\uDD1E)": "clef" }' -s <<'.'
(⣿)=braille
(𝄞)=clef
.

printf 'nul\0nul\tnul\0nul\n' > $TMP.nul	# bash doesn't do $'\000'
tc "NULs are correctly printed" \
assert_output '{ "nul\u0000nul": "nul\u0000nul" }' -st <$TMP.nul

printf 'nul\0nul\t"nul\\u0000nul"\n' > $TMP.nul
tc "NULs are correctly reversed" \
assert_output '{ "nul\u0000nul": "nul\u0000nul" }' -t <$TMP.nul

tc "We are relaxed about commas" \
assert_output '[,,,,,hey,{,"a":"b",,}]' <<'.'
[5]=hey
[6].a="b"
.

tc "Duplicate keys means duplicate output" \
assert_output '{"key":"value", "key":"another value"}' <<'.'
key="value"
key="another value"
.

tc "Non-structured values get an empty keypath" \
assert_output '1' <<'.'
=1
.

tc "Reversed empty keypath works" \
assert_reverse_output '1' <<'.'
=1
.

tc "Simple array reverse works" \
assert_reverse_output '[1]' <<'.'
[0]=1
.

tc "Can reverse combined object and array" \
assert_reverse_output '[{"a":"a","b":"b"},{"a":"c","b":"d"}]' <<'.'
[0].a="a"
[0].b="b"
[1].a="c"
[1].b="d"
.

tc "Missed array elements imply null" \
assert_reverse_output '[null,1]' <<'.'
[1]=1
.

tc "Later property lines override earlier" \
assert_reverse_output '{"a":5,"b":"hello"}' <<'.'
a=3
b="hello"
a=4
a=5
.

tc "Complicated implied array elements works" \
assert_reverse_output '{"a":[null,[null,null,{"b":true}]]}' <<'.'
a[1][2].b=true
.

tc "Deleting a property works" \
assert_reverse_output '{"bar":null}' <<'.'
foo[0]="true"
bar=null
foo
.

tc "Appending to an array works" \
assert_reverse_output '["a","b","c"]' <<'.'
[]="a"
[]="b"
[]="c"
.

tc "Deleting an element doesn't delete implied nulls" \
assert_reverse_output '{"a":[null,null,null]}' <<'.'
a[3]="killme"
a[-1]
.

tc "Deleting trailing elements shortens array" \
assert_reverse_output '{"a":[null,null,"success"]}' <<'.'
a[3]="killme"
a[3]
a[2]
a[]="success"
.

