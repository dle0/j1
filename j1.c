/*
 * Converts JSON into a linear form.
 *
 * For example, the input:
 *
 *   { "foo": [ true, { "height": 9.3 } ], "bar": null }
 *
 * will be converted to:
 *
 *   foo[0]="true"
 *   foo[1].height=9.3
 *   bar=null
 *
 * To make scripting easier, option -s will remove quotes from strings,
 * option -t will use a TAB instead of '=' separator, and option -0 will
 * use NULs instead of \n.
 *
 *
 * Reverse mode (-r)
 *
 * Lines are read to build up a JSON value in memory, which is printed
 * when input ends. Because later lines can override earlier lines, you can
 * use j1 -r to apply patches to JSON values.
 *
 * In reverse mode, the following path forms are special:
 *   []      appends to an array
 *   [-1]    refers to the last element of the array
 *
 * If a line is missing its separator it is interpreted as deleting the
 * named path. For example, this linear input:
 *
 *   foo[0]="true"
 *   bar=null
 *   foo
 *
 * produces the JSON output:
 *
 *   {"bar":null}
 */

#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <stdbool.h>
#include <unistd.h>

static int G_separator = '=';	/* linear form separator */
static int G_eol = '\n';	/* linear form terminator */
static bool G_strings_unquoted;	/* linear string values are unquoted */

static bool
streq(const char *a, const char *b)
{
	return a && b && strcmp(a, b) == 0;
}

/* x*alloc() functions die immediately on error */

static void *
xrealloc(void *p, size_t sz)
{
	p = realloc(p, sz);
	if (!p && sz)
		err(1, "realloc");
	return p;
}

static char *
xstrndup(const char *s, int len)
{
	char *cp = malloc(len + 1);
	if (!cp)
		errx(1, "malloc");
	memcpy(cp, s, len);
	cp[len] = '\0';
	return cp;
}

static char *
xstrdup(const char *s)
{
	return xstrndup(s, strlen(s));
}

/*
 * file: a stdio wrapper that counts lines and
 *       remembers filenames for better error messages
 */
struct file {
	FILE *f;
	const char *name;
	unsigned int lineno;
	unsigned int chpos;
	bool was_eol;	/* Next char read will be bol */
	int lastch;	/* last value returned by file_getc() */
};
#define FMT_file "%s:%u:%u"
#define ARG_file(f) (f)->name, (f)->lineno, (f)->chpos

static void
file_reset(struct file *file)
{
	file->was_eol = true;
	file->lineno = 0;
	file->chpos = 0;
}

static void
file_close(struct file *file)
{
	if (file->f) {
		fclose(file->f);
		file->f = NULL;
		file->name = NULL;
	}
}

static bool
file_open(struct file *f, const char *path)
{
	if (streq(path, "-")) {
		f->name = "<stdin>";
		f->f = stdin;
	} else {
		f->name = path;
		f->f = fopen(path, "r");
	}
	file_reset(f);
	return !!f->f;
}

/* getc, with lineno tracking */
static int
file_getc(struct file *file)
{
	int ch = fgetc(file->f);
	if (ch == EOF)
		return EOF;
	if (file->was_eol) {
		file->was_eol = false;
		file->chpos = 0;
		file->lineno++;
	}
	if (ch != G_eol)
		file->chpos++;
	file->was_eol = (ch == G_eol);
	return file->lastch = ch;
}

static void
file_ungetc(struct file *file, int ch)
{
	if (ch != EOF) {
		ungetc(ch, file->f);
		if (file->chpos)
			file->chpos--;
	}
}

static bool
file_eof(struct file *file)
{
	int ch = fgetc(file->f);
	if (ch != EOF)
		ungetc(ch, file->f);
	return ch == EOF;
}

static int
file_lastch(struct file *file)
{
	return file->lastch;
}

/*
 * str: accumulates characters into a C string, using realloc
 */
struct str {
	size_t alloc;
	size_t len;
	char *s;		/* NULL, or a C string */
};
#define STR_INIT { 0, 0, NULL }
#define STR_ALLOC_INC 64	/* realloc step size */

/* Returns the C string form of the buffer */
static const char *
str(struct str *str)
{
	return str->alloc ? str->s : "";
}

/* Grows the str by n bytes.
 * Allocation is forced even if n==0.
 * Returns pointer to the uninitialised bytes. */
static char *
str_append_pad(struct str *str, size_t n)
{
	while (str->len + n >= str->alloc)
		str->s = xrealloc(str->s, str->alloc += STR_ALLOC_INC);
	char *ret = str->s + str->len;
	str->s[str->len += n] = '\0';
	return ret;
}

static void
str_append(struct str *str, char ch)
{
	*str_append_pad(str, 1) = ch;
}

/* Resets the string length to 0. The allocation is unchanged. */
static void
str_reset(struct str *str)
{
	str->len = 0;
	if (str->alloc)
		str->s[0] = '\0';
}

/* Destructs str */
static void
str_fini(struct str *str)
{
	if (str->alloc) {
		free(str->s);
		str->s = NULL;
		str->alloc = 0;
		str->len = 0;
	}
}


/*
 * Reads a JSON token into @token, skipping leading whitespace.
 * Tokens that can be returned are: [ ] { } , : "string", word.
 * Where a 'word' is any sequence excluding []{},:" and whitespace.
 * Returns 0 iff EOF.
 */
static size_t
get_token(struct str *token, struct file *file)
{
	int ch;

	str_reset(token);
	while ((ch = file_getc(file)) != EOF && isspace(ch))
		; /* skip */

	if (ch == EOF)
		return 0;

	str_append(token, ch);
	if (strchr("[]{},:", ch))
		goto out;

	bool quoted = (ch == '"');
	bool escaped = false;
	while ((ch = file_getc(file)) != EOF) {
		if (!quoted && strchr("[]{},:", ch)) {
			file_ungetc(file, ch);
			break;
		}
		if (quoted && ch == '\n')
			warnx(FMT_file ": unescaped newline in string value",
			      ARG_file(file));

		if (!quoted && isspace(ch))
			break;
		str_append(token, ch);
		if (quoted) {
			if (escaped)
				escaped = false;
			else if (ch == '"') {
				quoted = false;
				break;
			} else if (ch == '\\')
				escaped = true;
		}
	}
	if (quoted)
		errx(1, FMT_file ": unexpected EOF in string", ARG_file(file));
out:
	return token->len;
}

/*
 * Prints the content of a JSON string @js as UTF-8 to @out.
 * The string is first unescaped (and UTF-8 surrogates are flattened)
 * and then the following characters are escaped into an \xHH form:
 *  \    -> \\
 *  lf   -> \n
 *  G_eol -> \x..
 *  G_separator -> \x..	 (only when
 *  [    -> \x5b	 (only when IS_KEY)
 *  ]    -> \x5d	 (only when IS_KEY)
 *  .    -> \x2e	 (only when IS_KEY)
 *  whitespace -> \x..	 (only when IS_KEY or G_separator='=')
 *
 * When G_separator is not '=', we assume that the output is
 * going to awk which does field-splitting; so this function will
 * escape all instances of G_separator in that case.
 * Otherwise, when G_separator is '=', we assume that the output
 * is for a setenv-like consumer that only looks for the first '=',
 * but is probably sensitive to whitespace.
 */
#define IS_KEY 1
static void
fput_unquoted_json_string(const char *js, FILE *out, int flags)
{
	assert(*js == '"');
	js++;
	while (*js && *js != '"') {
		unsigned long ch = *js++ & 0xff;
		if (ch == '\\' && *js) {
			ch = *js++;
			if (ch == 'u' && isxdigit(js[0]) && isxdigit(js[1]) &&
			    isxdigit(js[2]) && isxdigit(js[3]) &&
			    sscanf(js, "%4lx", &ch) == 1)
				js += 4;
			else if (ch == 'b') ch = 0x0008;
			else if (ch == 'f') ch = 0x000c;
			else if (ch == 'n') ch = 0x000a;
			else if (ch == 'r') ch = 0x000d;
			else if (ch == 't') ch = 0x0009;
		}

		/* Decode surrogate pairs */
		unsigned long ch2;
		if ((ch & 0xfc00) == 0xd800 && js[0] == '\\' && js[1] == 'u' &&
		    isxdigit(js[2]) && isxdigit(js[3]) && isxdigit(js[4]) &&
		    isxdigit(js[5]) && sscanf(js + 2, "%4lx", &ch2) == 1 &&
		    (ch2 & 0xfc00) == 0xdc00)
		{
			ch = 0x10000 | (ch - 0xd800) << 10 | (ch2 - 0xdc00);
			js += 6;
		}

		/* Emit as UTF-8 or \xXX */
		if (ch == '\\' || ch == G_eol ||
		    (ch == G_separator && G_separator != '=') ||
		    ((flags & IS_KEY) &&
		     (ch == '.' || ch == '[' || ch == ']' ||
		      (ch <= ' ' && G_separator == '=') ||
		      ch == G_separator)))
		{
			/* Forced escape */
			switch (ch) {
			case '\0': fputs("\\0", out); break;
			case '\b': fputs("\\b", out); break;
			case '\t': fputs("\\t", out); break;
			case '\n': fputs("\\n", out); break;
			case '\f': fputs("\\f", out); break;
			case '\r': fputs("\\r", out); break;
			case '\\': fputs("\\\\", out); break;
			default: fprintf(out, "\\x%02x", (unsigned char)ch);
			}
		} else if (ch < 0x80) {
			putc(0x00 | ((ch >>  0) & 0x7f), out);
		} else if (ch < 0x800) {
			putc(0xc0 | ((ch >>  6) & 0x1f), out);
			putc(0x80 | ((ch >>  0) & 0x3f), out);
		} else if (ch < 0x10000) {
			putc(0xe0 | ((ch >> 12) & 0x0f), out);
			putc(0x80 | ((ch >>  6) & 0x3f), out);
			putc(0x80 | ((ch >>  0) & 0x3f), out);
		} else {
			putc(0xf0 | ((ch >> 18) & 0x07), out);
			putc(0x80 | ((ch >> 12) & 0x3f), out);
			putc(0x80 | ((ch >>  6) & 0x3f), out);
			putc(0x80 | ((ch >>  0) & 0x3f), out);
		}
	}
}

/*
 * Converts raw UTF-8 string @s into a quoted JSON string in @out.
 */
static void
to_json_string(const char *s, struct str *out)
{
	str_reset(out);
	str_append(out, '"');
	for (; *s; s++) {
		switch (*s) {
		case 0x08: str_append(out, '\\'); str_append(out, 'b'); break;
		case 0x09: str_append(out, '\\'); str_append(out, 't'); break;
		case 0x0a: str_append(out, '\\'); str_append(out, 'n'); break;
		case 0x0c: str_append(out, '\\'); str_append(out, 'f'); break;
		case 0x0d: str_append(out, '\\'); str_append(out, 'r'); break;
		case '"':
		case '\\': str_append(out, '\\'); /* fallthough */
		default:
			if (*s >= ' ')
				str_append(out, *s);
			else
				snprintf(str_append_pad(out, 6), 7,
					"\\u%04X", *s);;
		}
	}
	str_append(out, '"');
}

/* A step in a JSON structure path.
 * Each step is either an object key or an array index. */
struct step {
	struct step *next, *prev;
	enum step_type { KEY, INDEX } type;
	union {
		char *key;
		unsigned int index;
	};
};


static struct step *
make_step(enum step_type type)
{
	struct step *step = malloc(sizeof *step);
	if (!step) err(1, "malloc");
	step->type = type;
	step->key = NULL;
	if (type == INDEX)
		step->index = 0;
	else
		step->key = NULL;
	return step;
}

static void
free_step(struct step *step)
{
	if (step && step->type == KEY)
		free(step->key);
	free(step);
}


/* A structure path is a list of steps */
struct path {
	struct step *first, *last;
};

static void
path_append(struct path *path, struct step *step)
{
	step->next = NULL;
	step->prev = path->last;
	if (!path->last)
		path->first = path->last = step;
	else
		path->last = (path->last->next = step);
}

static struct step *
path_pop(struct path *path)
{
	struct step *last = path->last;
	if (last) {
		path->last = last->prev;
		if (path->last)
			path->last->next = NULL;
		else
			path->first = NULL;
	}
	return last;
}

/* Read a JSON value from @file and print line assignments to @out */
static void
from_json(struct file *file, FILE *out)
{
	struct path path = { 0 };
	struct str token_str = STR_INIT;
	size_t tlen;
	bool seencolon = false;
	bool seencontent = true;

	while ((tlen = get_token(&token_str, file))) {
		char *token = token_str.s;
		if (*token == ',') {
			seencolon = false;
			if (path.last && path.last->type == INDEX)
			        path.last->index++;
		} else if (*token == ':') {
			seencolon = true;
		} else if (*token == '[' || *token == '{') {
			struct step *step = make_step(*token == '[' ? INDEX : KEY);
			path_append(&path, step);
			seencolon = false;
			seencontent = false;
		} else if (*token == ']' || *token == '}') {
			if (!path.last || path.last->type != (*token == ']' ? INDEX : KEY))
				errx(1, FMT_file ": error unexpected '%s'",
					ARG_file(file), token);
			free_step(path_pop(&path));
			if (!seencontent) {
				/* When a property is [] or {}, then force a
				 * line to be emitted. Otherwise, we will lose
				 * information when reversing. */
				token = (*token == ']') ? "[]" : "{}";
				goto lineout;
			}
		} else if (path.last && path.last->type == KEY && !seencolon) {
			/* record the "key": part of an object property */
			free(path.last->key);
			path.last->key = xstrndup(token, tlen);
		} else {
lineout:
			seencontent = true;
			seencolon = false;
			for (struct step *step = path.first; step; step = step->next) {
			    if (step->type == KEY) {
				if (step != path.first)
					fputc('.', out);
				if (*step->key == '"')
					fput_unquoted_json_string(step->key, out, IS_KEY);
				else
					fputs(step->key, out);
			    } else {
				fprintf(out, "[%u]", step->index);
			    }
			}
			fputc(G_separator, out);
			if (*token == '"' && G_strings_unquoted)
				fput_unquoted_json_string(token, out, 0);
			else
				fputs(token, out);
			fputc(G_eol, out);
		}
	}
	if (path.last) {
		warnx(FMT_file ": truncated", ARG_file(file));
		while (path.last)
			free_step(path_pop(&path));
	}
	str_fini(&token_str);
}

/* A representation of a JSON value */
struct json {
	enum json_type { JSON_UNDEFINED, JSON_LITERAL, JSON_ARRAY, JSON_OBJECT } type;
	struct json *next;
	union {
		char *literal;			/* numbers, strings, boolean etc */
		struct {
			unsigned int len;
			struct json *elements;	/* allocated array */
		} array;
		struct {
			unsigned int len;	/* twice number of keys */
			struct json *keyvalues;	/* keys at even, value at odd */
		} object;
	};
};

static void
json_print(struct json *json, FILE *out, struct str *buf)
{
	int count = 0;
	switch (json->type) {
	case JSON_UNDEFINED:
		fputs("null", out);
		break;
	case JSON_LITERAL:
		fputs(json->literal, out);
		break;
	case JSON_ARRAY:
		putc('[', out);
		for (int i = 0; i < json->array.len; i++) {
			if (json->array.elements[i].type == JSON_UNDEFINED)
				continue; /* deleted element */
			if (count++)
				putc(',', out);
			json_print(&json->array.elements[i], out, buf);
		}
		putc(']', out);
		break;
	case JSON_OBJECT:
		putc('{', out);
		for (int i = 0; i < json->object.len; i+=2) {
			struct json *key = &json->object.keyvalues[i];
			struct json *value = &json->object.keyvalues[i + 1];
			if (value->type == JSON_UNDEFINED)
				continue; /* deleted key */
			if (count++)
				putc(',', out);
			to_json_string(key->literal, buf);
			fputs(buf->s, out);
			putc(':', out);
			json_print(value, out, buf);
		}
		putc('}', out);
		break;
	}
}

/* Forces a json value to undefined, releasing any prior resources.
 * This resets a json structure so that its fields can be assigned directly */
static void
json_undef(struct json *json)
{
	switch (json->type) {
	case JSON_UNDEFINED:
		break;
	case JSON_LITERAL:
		free(json->literal);
		break;
	case JSON_ARRAY:
		for (int i = 0; i < json->array.len; i++)
			json_undef(&json->array.elements[i]);
		free(json->array.elements);
		break;
	case JSON_OBJECT:
		for (int i = 0; i < json->object.len; i++)
			json_undef(&json->object.keyvalues[i]);
		free(json->object.keyvalues);
		break;
	}
	json->type = JSON_UNDEFINED;
}

static struct json *
json_array_find(struct json *json, int index)
{
	if (json->type != JSON_ARRAY)
		json_undef(json);
	if (json->type == JSON_UNDEFINED) {
		json->type = JSON_ARRAY;
		json->array.elements = NULL;
		json->array.len = 0;
	}
	if (index >= json->array.len) {
		int newlen = index + 1;
		json->array.elements = xrealloc(json->array.elements,
			newlen * sizeof json->array.elements[0]);
		for (int i = json->array.len; i < newlen; i++) {
			json->array.elements[i].type = JSON_LITERAL;
			json->array.elements[i].literal = xstrdup("null");
		}
		json->array.len = newlen;
	}
	return &json->array.elements[index];
}

/* Return length of JSON array with trailing undefineds removed */
static int
json_array_len(struct json *json)
{
	if (json->type != JSON_ARRAY)
		return 0;
	int alen = json->array.len;
	while (alen && json->array.elements[alen - 1].type == JSON_UNDEFINED)
		alen--;
	return alen;
}

static struct json *
json_object_find(struct json *json, const char *key)
{
	if (json->type != JSON_OBJECT)
		json_undef(json);
	if (json->type == JSON_UNDEFINED) {
		json->type = JSON_OBJECT;
		json->object.keyvalues = NULL;
		json->object.len = 0;
	}
	for (int i = 0; i < json->object.len; i += 2)
		if (streq(json->object.keyvalues[i].literal, key))
			return &json->object.keyvalues[i+1];
	json->object.keyvalues = xrealloc(json->object.keyvalues,
		(json->object.len + 2) * sizeof json->object.keyvalues[0]);
	struct json *kv = &json->object.keyvalues[json->object.len];
	kv[0].type = JSON_LITERAL;
	kv[0].literal = xstrdup(key);
	kv[1].type = JSON_UNDEFINED;
	json->object.len += 2;
	return &kv[1];
}

/* Scans in a json path (up to but not including @G_separator or @G_eol),
 * and then locates or constructs the corresponding leaf under the @root.
 * Returns a pointer to the located or constructed node (it may be undefined).
 * Returns NULL on unexpeted EOF.
 */
static struct json *
scan_json_path(struct file *file, struct json *root, struct str *buf)
{
	int ch;
	str_reset(buf);
	struct json *json = root;
	while ((ch = file_getc(file)) != EOF) {
		if (!(ch == G_separator ||
		      ch == G_eol ||
		      ch == '.' ||
		      (ch == '[' && buf->len)))
		{
			str_append(buf, ch);
			if (ch != ']')
				continue;
		}

		if (ch == ']') {
			int index;
			int alen = json_array_len(json);
			if (streq(buf->s, "[]")) {	/* Array append */
				index = alen;
			} else if (sscanf(buf->s, "[%d]", &index) == 1) {
				if (index < 0) {
					index = alen + index;
					if (index < 0)
						index = 0;
				}
			} else
				errx(1, FMT_file ": error bad index '%s'",
					ARG_file(file), buf->s);
			json = json_array_find(json, index);
		} else if (buf->len) {
			json = json_object_find(json, str(buf));
		} /* else empty path */
		str_reset(buf);
		if (ch == '[')
			str_append(buf, ch);
		else if (ch == G_separator || ch == G_eol)
			break;
	}
	return ch == EOF ? NULL : json;
}

/* test for strict JSON number representation */
static bool
is_json_number(const char *s)
{
	if (*s == '-')
		s++;
	if (*s == '0')
		s++;
	else {
		/* 1*DIGIT */
		if (!isdigit(*s)) return false;
		while (isdigit(*s)) s++;
	}
	if (*s == '.') {
		s++;
		if (!isdigit(*s)) return false;
		while (isdigit(*s)) s++;
	}
	if (*s == 'e' || *s == 'E') {
		s++;
		if (*s == '+' || *s == '-')
			s++;
		if (!isdigit(*s)) return false;
		while (isdigit(*s)) s++;
	}
	return !*s;
}

/* Scans @file up to end-of-line, and insert the simple value into the JSON
 * tree at @jsonp.
 * If the value does not look like valid JSON, and G_strings_unquoted is in
 * effect then the value is quoted to become a JSON string.
 * Returns true on success.
 */
static bool
scan_json_value(struct file *file, struct json *json, struct str *buf)
{
	int ch;
	str_reset(buf);
	while ((ch = file_getc(file)) != EOF && ch != G_eol)
		str_append(buf, ch);
	if (ch == EOF)
		return false;

	bool is_literal;
	if (!buf->len)
		is_literal = false;
	else if (buf->len >= 2 &&
		 buf->s[0] == '"' &&
		 buf->s[buf->len - 1] == '"')
		is_literal = true;
	else
		is_literal =
			is_json_number(buf->s) ||
			streq(buf->s, "true") ||
			streq(buf->s, "false") ||
			streq(buf->s, "null") ||
			streq(buf->s, "[]") ||
			streq(buf->s, "{}");

	char *literal;
	if (is_literal) {
		literal = xstrdup(str(buf));
	} else if (!G_strings_unquoted) {
		warnx(FMT_file ": warning: invalid JSON", ARG_file(file));
		literal = xstrdup(str(buf));
	} else {
		struct str quoted = STR_INIT;
		to_json_string(buf->s, &quoted);
		literal = xstrdup(quoted.s);
		str_fini(&quoted);
	}

	json_undef(json);
	json->type = JSON_LITERAL;
	json->literal = literal;

	return true;
}

/* Scans each line <path>=<value> from @file, and
 * then print to @out the corresponding JSON value */
static void
to_json(struct file *file, FILE *out)
{
	struct json root = { JSON_UNDEFINED };
	struct str buf = STR_INIT;

	while (!file_eof(file)) {
		struct json *json = scan_json_path(file, &root, &buf);
		if (!json) {
			warnx(FMT_file ": error: unexpected EOF", ARG_file(file));
			break;
		}
		if (file_lastch(file) == G_separator) {
			if (!scan_json_value(file, json, &buf))
				errx(1, FMT_file ": error: bad value '%s'",
					ARG_file(file), str(&buf));
		} else /* lastch == G_eol */ {
			json_undef(json);
		}
		if (0) {
			fprintf(stderr, FMT_file ": ", ARG_file(file));
			json_print(&root, stderr, &buf);
			putc('\n', stderr);
		}
	}

	if (root.type == JSON_UNDEFINED && G_strings_unquoted) {
		/* -s preserves empty input */
	} else {
		json_print(&root, out, &buf);
		putc('\n', out);
		json_undef(&root);
	}
}

int
main(int argc, char *argv[])
{
	int opt;
	bool error = false;
	bool reverse = false;

	while ((opt = getopt(argc, argv, "0rst")) != -1) {
		switch (opt) {
		case '0':
			G_eol = '\0';
			break;
		case 'r':
			reverse = true;		/* reverse mode */
			break;
		case 's':
			G_strings_unquoted = true;/* string values unquoted */
			break;
		case 't':
			G_separator = '\t';	/* TAB separator, not "=" */
			break;
		default:
			error = true;
		}
	}
	if (error) {
		fprintf(stderr, "usage: %s [-0rst] [file...]\n"
				"\t-0  terminate lines with NUL, not LF\n"
				"\t-r  reverse\n"
				"\t-s  string values are unquoted\n"
				"\t-t  TAB, not '=', is the separator\n"
				"", argv[0]);
#ifdef VERSION
		fprintf(stderr, "version %s\n", VERSION);
#endif
		exit(1);
	}

	if (optind >= argc) {
		static const char* default_args[] = { "-" };
		argv = (char **)default_args;
		optind = 0;
		argc = 1;
	}

	while (optind < argc) {
		struct file file;
		if (!file_open(&file, argv[optind++]))
			err(1, "%s", file.name);
		if (reverse)
			to_json(&file, stdout);
		else
			from_json(&file, stdout);
		file_close(&file);
	}
}
